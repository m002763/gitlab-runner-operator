#!/usr/bin/env python3

import re
import sys

version = sys.argv[1]
filepath = f'community-operator/{version}/bundle.Dockerfile'

with open(filepath, 'r+') as f:
    content = f.read()
    content_new = re.sub('^COPY bundle/', r'COPY ', content, flags=re.M)
    f.seek(0)
    f.write(content_new)
    f.truncate()
